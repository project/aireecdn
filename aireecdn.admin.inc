<?php
/**
 * @file
 * Administration functions for the Airee CDN module.
 */


/**
 * Administration settings form.
 */
function aireecdn_settings_form($form, &$form_state) {
  $form['aireecdn_balance'] = array(
    '#type' => 'item',
    '#title' => t('Balance'),
    '#description' => t('<a href="!url_balance" target="_blank">Airee balance</a>. First 5 Gb per day are free, then 5 RUR/Gb.',
      array('!url_balance' => 'https://xn--80aqc2a.xn--p1ai/my/site/balance/')),
    '#markup' => variable_get('aireecdn_balance', 500),
  );
  // TODO: add button to replenish balance.

  $form['aireecdn_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('API key for <a href="!url_account" target="_blank">Airee account</a>, you can get API key on <a href="!url_profile" target="_blank">profile page.</a>',
      array(
        '!url_account' => 'https://xn--80aqc2a.xn--p1ai/my/site/',
        '!url_profile' => 'https://xn--80aqc2a.xn--p1ai/my/site/profile/',
      )),
    '#default_value' => variable_get('aireecdn_apikey', ''),
  );

  $form['aireecdn_options'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Airee CDN options'),
    '#options' => array(
      'css' => t('CDN for CSS files'),
      'js' => t('CDN for JavaScript files'),
      'images' => t('CDN for images'),
      'media' => t('CDN for media files'),
    ),
    '#default_value' => variable_get('aireecdn_options', array()),
  );

  // TODO: add logic to disable/enable settings.
  $form['aireecdn_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Airee CDN domain'),
    '#default_value' => variable_get('aireecdn_domain', ''),
    '#disabled' => TRUE,
  );

  $timestamp = variable_get('aireecdn_timestamp_balance_check', '');
  $form['aireecdn_timestamp_balance_check'] = array(
    '#type' => 'item',
    '#title' => t('Last balance check'),
    '#markup' => format_date($timestamp),
    '#access' => $timestamp,
  );

  // TODO: check balance on submit.
  return system_settings_form($form);
}
